use sycamore::prelude::*;

fn main() {
    sycamore::render(|cx| {
        let state_signal = create_signal(cx, Scene::SceneA);
        provide_context_ref(cx, state_signal);
        view! {cx,
            PageContainer(state_signal=state_signal) {}
        }
    });
}

#[derive(PartialEq, Clone, Copy)]
pub enum Scene {
    SceneA,
    SceneB,
}

impl std::fmt::Display for Scene {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Scene::SceneA => write!(f, "SceneA"),
            Scene::SceneB => write!(f, "SceneB"),
        }
    }
}

#[derive(Prop)]
struct AppState<'cx, G: Html> {
    children: Children<'cx, G>,
    pub state_signal: &'cx Signal<Scene>,
}

#[component]
fn PageContainer<'a, G: Html>(cx: Scope<'a>, state: AppState<'a, G>) -> View<G> {
    view! { cx,
        div {

        (if *state.state_signal.get() == Scene::SceneB{
            view! {cx,
                    SceneB { }
                }

        } else {
            view! {cx,
                    SceneA {}
                }
            })
        }

    }
}

#[component]
fn SceneA<G: Html>(cx: Scope) -> View<G> {
    let sig = use_context::<Signal<Scene>>(cx);
    sig.set(Scene::SceneB);
    view! {cx,
        p {
    "You are now on SceneA. The current Scene Signal is: " (sig.get())}
    }
}

#[component]
fn SceneB<G: Html>(cx: Scope) -> View<G> {
    view! {cx,
        p {
        "You are now on SceneB"
        }
    }
}
